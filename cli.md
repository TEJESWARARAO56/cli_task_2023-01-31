git init
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
```
tejeswar@tejeswar-Aspire-A715-42G:/home$ cd ./tejeswar/
tejeswar@tejeswar-Aspire-A715-42G:~$ cd d1
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ touch a.txt
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ ls -l a.txt
-rw-rw-r-- 1 tejeswar tejeswar 0 Jan 31 15:21 a.txt
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ chmod 755 a.txt
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ mkdir d2
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ cd d2
tejeswar@tejeswar-Aspire-A715-42G:~/d1/d2$ touch b.txt
tejeswar@tejeswar-Aspire-A715-42G:~/d1/d2$ pwd
/home/tejeswar/d1/d2
tejeswar@tejeswar-Aspire-A715-42G:~/d1/d2$ cd ..
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ chmod 755 -R d2
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ firefox
Gtk-Message: 15:24:43.099: Not loading module "atk-bridge": The functionality is provided by GTK natively. Please try to not load it.
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ top

top - 15:25:35 up  1:41,  1 user,  load average: 0.63, 0.53, 0.67
Tasks: 378 total,   1 running, 377 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.7 us,  0.8 sy,  0.0 ni, 98.4 id,  0.0 wa,  0.0 hi,  0.1 si,  0.0 st
MiB Mem :   7294.5 total,    184.0 free,   4227.6 used,   2882.9 buff/cache
MiB Swap:  13311.0 total,  13310.0 free,      1.0 used.   2691.4 avail Mem 

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                         
   4936 tejeswar  20   0 8036028 631720 283528 S   7.9   8.5  21:57.31 zoom                            
   5490 tejeswar  20   0  109.0g 377700 145676 S   3.0   5.1   5:39.06 Discord                         
   1722 tejeswar  20   0   25.6g 174776  99040 S   2.3   2.3   4:05.51 Xorg                            
   5424 tejeswar  20   0  693724 176484  97584 S   2.3   2.4   2:18.03 Discord                         
   1990 tejeswar  20   0 6857996 300188 128828 S   1.3   4.0   5:05.14 gnome-shell                     
   8684 tejeswar  20   0  904004  52244  39204 S   1.0   0.7   0:06.35 gnome-terminal-                 
    266 root     -51   0       0      0      0 S   0.3   0.0   0:08.83 irq/43-ELAN050A                 
    589 systemd+  20   0   14824   6020   5216 S   0.3   0.1   0:07.72 systemd-oomd                    
   1603 tejeswar   9 -11 3865636  25292  17880 S   0.3   0.3   2:37.39 pulseaudio                      
   2094 tejeswar  20   0  162744   8044   7248 S   0.3   0.1   0:00.79 at-spi2-registr                 
   5390 tejeswar  20   0   36.6g 155348 106552 S   0.3   2.1   0:35.70 Discord                         
   6338 tejeswar  20   0 2653472 147604  95580 S   0.3   2.0   0:07.22 Isolated Web Co                 
  10423 tejeswar  20   0   32.5g 248372 174472 S   0.3   3.3   0:33.64 chrome                          
      1 root      20   0  166836  11684   7984 S   0.0   0.2   0:01.51 systemd                         
      2 root      20   0       0      0      0 S   0.0   0.0   0:00.01 kthreadd                        
      3 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 rcu_gp                          
      4 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 rcu_par_gp                      
      5 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 netns                           
      7 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 kworker/0:0H-events_highpri     
     10 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 mm_percpu_wq                    
     11 root      20   0       0      0      0 S   0.0   0.0   0:00.00 rcu_tasks_rude_                 
     12 root      20   0       0      0      0 S   0.0   0.0   0:00.00 rcu_tasks_trace                 
     13 root      20   0       0      0      0 S   0.0   0.0   0:00.15 ksoftirqd/0                     
     14 root      20   0       0      0      0 I   0.0   0.0   0:06.06 rcu_sched                       
     15 root      rt   0       0      0      0 S   0.0   0.0   0:00.02 migration/0                     
     16 root     -51   0       0      0      0 S   0.0   0.0   0:00.00 idle_inject/0                   
     17 root      20   0       0      0      0 S   0.0   0.0   0:00.00 cpuhp/0                         
     18 root      20   0       0      0      0 S   0.0   0.0   0:00.00 cpuhp/1                         
     19 root     -51   0       0      0      0 S   0.0   0.0   0:00.00 idle_inject/1                   
     20 root      rt   0       0      0      0 S   0.0   0.0   0:00.24 migration/1                     
     21 root      20   0       0      0      0 S   0.0   0.0   0:00.14 ksoftirqd/1                     
     23 root       0 -20       0      0      0 I   0.0   0.0   0:00.00 kworker/1:0H-events_highpri     
     24 root      20   0       0      0      0 S   0.0   0.0   0:00.00 cpuhp/2                         
     25 root     -51   0       0      0      0 S   0.0   0.0   0:00.00 idle_inject/2                   
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root           1  0.0  0.1 166836 11684 ?        Ss   13:43   0:01 /sbin/init splash
root           2  0.0  0.0      0     0 ?        S    13:43   0:00 [kthreadd]
root           3  0.0  0.0      0     0 ?        I<   13:43   0:00 [rcu_gp]
root           4  0.0  0.0      0     0 ?        I<   13:43   0:00 [rcu_par_gp]
root           5  0.0  0.0      0     0 ?        I<   13:43   0:00 [netns]
root           7  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/0:0H-events_highpri]
root          10  0.0  0.0      0     0 ?        I<   13:43   0:00 [mm_percpu_wq]
root          11  0.0  0.0      0     0 ?        S    13:43   0:00 [rcu_tasks_rude_]
root          12  0.0  0.0      0     0 ?        S    13:43   0:00 [rcu_tasks_trace]
root          13  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/0]
root          14  0.0  0.0      0     0 ?        I    13:43   0:06 [rcu_sched]
root          15  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/0]
root          16  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/0]
root          17  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/0]
root          18  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/1]
root          19  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/1]
root          20  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/1]
root          21  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/1]
root          23  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/1:0H-events_highpri]
root          24  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/2]
root          25  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/2]
root          26  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/2]
root          27  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/2]
root          29  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/2:0H-kblockd]
root          30  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/3]
root          31  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/3]
root          32  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/3]
root          33  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/3]
root          35  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/3:0H-events_highpri]
root          36  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/4]
root          37  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/4]
root          38  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/4]
root          39  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/4]
root          41  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/4:0H-kblockd]
root          42  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/5]
root          43  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/5]
root          44  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/5]
root          45  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/5]
root          47  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/5:0H-events_highpri]
root          48  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/6]
root          49  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/6]
root          50  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/6]
root          51  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/6]
root          53  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/6:0H-events_highpri]
root          54  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/7]
root          55  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/7]
root          56  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/7]
root          57  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/7]
root          59  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/7:0H-events_highpri]
root          60  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/8]
root          61  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/8]
root          62  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/8]
root          63  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/8]
root          65  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/8:0H-events_highpri]
root          66  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/9]
root          67  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/9]
root          68  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/9]
root          69  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/9]
root          71  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/9:0H-events_highpri]
root          72  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/10]
root          73  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/10]
root          74  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/10]
root          75  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/10]
root          77  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/10:0H-events_highpri]
root          78  0.0  0.0      0     0 ?        S    13:43   0:00 [cpuhp/11]
root          79  0.0  0.0      0     0 ?        S    13:43   0:00 [idle_inject/11]
root          80  0.0  0.0      0     0 ?        S    13:43   0:00 [migration/11]
root          81  0.0  0.0      0     0 ?        S    13:43   0:00 [ksoftirqd/11]
root          83  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/11:0H-events_highpri]
root          84  0.0  0.0      0     0 ?        S    13:43   0:00 [kdevtmpfs]
root          85  0.0  0.0      0     0 ?        I<   13:43   0:00 [inet_frag_wq]
root          86  0.0  0.0      0     0 ?        S    13:43   0:00 [kauditd]
root          87  0.0  0.0      0     0 ?        S    13:43   0:00 [khungtaskd]
root          88  0.0  0.0      0     0 ?        S    13:43   0:00 [oom_reaper]
root          89  0.0  0.0      0     0 ?        I<   13:43   0:00 [writeback]
root          90  0.0  0.0      0     0 ?        S    13:43   0:00 [kcompactd0]
root          91  0.0  0.0      0     0 ?        SN   13:43   0:00 [ksmd]
root          92  0.0  0.0      0     0 ?        SN   13:43   0:00 [khugepaged]
root         139  0.0  0.0      0     0 ?        I<   13:43   0:00 [kintegrityd]
root         140  0.0  0.0      0     0 ?        I<   13:43   0:00 [kblockd]
root         141  0.0  0.0      0     0 ?        I<   13:43   0:00 [blkcg_punt_bio]
root         142  0.0  0.0      0     0 ?        I<   13:43   0:00 [tpm_dev_wq]
root         143  0.0  0.0      0     0 ?        I<   13:43   0:00 [ata_sff]
root         144  0.0  0.0      0     0 ?        I<   13:43   0:00 [md]
root         145  0.0  0.0      0     0 ?        I<   13:43   0:00 [edac-poller]
root         146  0.0  0.0      0     0 ?        I<   13:43   0:00 [devfreq_wq]
root         147  0.0  0.0      0     0 ?        S    13:43   0:00 [watchdogd]
root         149  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/0:1H-kblockd]
root         150  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/25-AMD-Vi]
root         152  0.0  0.0      0     0 ?        S    13:43   0:00 [kswapd0]
root         153  0.0  0.0      0     0 ?        S    13:43   0:00 [ecryptfs-kthrea]
root         155  0.0  0.0      0     0 ?        I<   13:43   0:00 [kthrotld]
root         157  0.0  0.0      0     0 ?        I    13:43   0:01 [kworker/1:1-events]
root         166  0.0  0.0      0     0 ?        I<   13:43   0:00 [acpi_thermal_pm]
root         169  0.0  0.0      0     0 ?        I<   13:43   0:00 [vfio-irqfd-clea]
root         172  0.0  0.0      0     0 ?        I<   13:43   0:00 [mld]
root         173  0.0  0.0      0     0 ?        I<   13:43   0:00 [ipv6_addrconf]
root         175  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/5:1H-kblockd]
root         183  0.0  0.0      0     0 ?        I<   13:43   0:00 [kstrp]
root         186  0.0  0.0      0     0 ?        I<   13:43   0:00 [zswap-shrink]
root         194  0.0  0.0      0     0 ?        I<   13:43   0:00 [charger_manager]
root         228  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/8:1H-kblockd]
root         259  0.0  0.0      0     0 ?        I<   13:43   0:00 [nvme-wq]
root         261  0.0  0.0      0     0 ?        I<   13:43   0:00 [nvme-reset-wq]
root         262  0.0  0.0      0     0 ?        I<   13:43   0:00 [nvme-delete-wq]
root         264  0.0  0.0      0     0 ?        S    13:43   0:00 [scsi_eh_0]
root         265  0.0  0.0      0     0 ?        I<   13:43   0:00 [scsi_tmf_0]
root         266  0.1  0.0      0     0 ?        S    13:43   0:10 [irq/43-ELAN050A]
root         268  0.0  0.0      0     0 ?        S    13:43   0:00 [scsi_eh_1]
root         269  0.0  0.0      0     0 ?        I<   13:43   0:00 [scsi_tmf_1]
root         274  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/7:1H-kblockd]
root         278  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/1:1H-kblockd]
root         279  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/11:1H-kblockd]
root         280  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/9:1H-kblockd]
root         281  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/3:1H-kblockd]
root         300  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/2:1H-kblockd]
root         302  0.0  0.0      0     0 ?        S    13:43   0:00 [jbd2/nvme0n1p5-]
root         303  0.0  0.0      0     0 ?        I<   13:43   0:00 [ext4-rsv-conver]
root         344  0.0  0.5  79548 43584 ?        S<s  13:43   0:00 /lib/systemd/systemd-journald
root         388  0.0  0.0  26604  6404 ?        Ss   13:43   0:00 /lib/systemd/systemd-udevd
root         391  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/4:1H-kblockd]
root         392  0.0  0.0      0     0 ?        I<   13:43   0:00 [ipmi-msghandler]
root         412  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/10:1H-kblockd]
root         416  0.0  0.0      0     0 ?        I<   13:43   0:00 [kworker/6:1H-kblockd]
root         466  0.0  0.0      0     0 ?        I<   13:43   0:00 [cfg80211]
root         467  0.0  0.0      0     0 ?        I<   13:43   0:00 [cryptd]
root         495  0.0  0.0      0     0 ?        S    13:43   0:03 [irq/77-iwlwifi:]
root         496  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/78-iwlwifi:]
root         497  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/79-iwlwifi:]
root         499  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/80-iwlwifi:]
root         500  0.1  0.0      0     0 ?        S    13:43   0:07 [irq/81-iwlwifi:]
root         501  0.0  0.0      0     0 ?        S    13:43   0:03 [irq/82-iwlwifi:]
root         502  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/83-iwlwifi:]
root         503  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/84-iwlwifi:]
root         504  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/85-iwlwifi:]
root         505  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/86-iwlwifi:]
root         506  0.0  0.0      0     0 ?        S    13:43   0:01 [irq/87-iwlwifi:]
root         507  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/88-iwlwifi:]
root         508  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/89-iwlwifi:]
root         509  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/90-iwlwifi:]
root         523  0.0  0.0      0     0 ?        S    13:43   0:00 [jbd2/nvme0n1p6-]
root         524  0.0  0.0      0     0 ?        I<   13:43   0:00 [ext4-rsv-conver]
root         551  0.0  0.0      0     0 ?        I    13:43   0:01 [kworker/2:2-events]
systemd+     589  0.1  0.0  14824  6020 ?        Ss   13:43   0:08 /lib/systemd/systemd-oomd
systemd+     590  0.0  0.1  25656 13948 ?        Ss   13:43   0:01 /lib/systemd/systemd-resolved
systemd+     591  0.0  0.0  89376  6564 ?        Ssl  13:43   0:00 /lib/systemd/systemd-timesyncd
root         622  0.0  0.0      0     0 ?        I<   13:43   0:05 [kworker/u33:2-rb_allocator]
root         623  0.0  0.0      0     0 ?        I<   13:43   0:00 [amd_iommu_v2]
root         627  0.0  0.0      0     0 ?        I    13:43   0:01 [kworker/3:3-events]
root         629  0.0  0.0      0     0 ?        S    13:43   0:00 [nv_queue]
root         630  0.0  0.0      0     0 ?        S    13:43   0:00 [nv_queue]
root         645  0.0  0.0      0     0 ?        S    13:43   0:00 [nvidia-modeset/]
root         646  0.0  0.0      0     0 ?        S    13:43   0:00 [nvidia-modeset/]
root         653  0.0  0.0      0     0 ?        S    13:43   0:00 [irq/95-nvidia]
root         654  0.0  0.0      0     0 ?        S    13:43   0:00 [nvidia]
root         655  0.0  0.0      0     0 ?        S    13:43   0:00 [nv_queue]
root         671  0.4  0.0      0     0 ?        S    13:43   0:27 [gfx]
root         672  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.0.0]
root         673  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.1.0]
root         674  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.2.0]
root         675  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.3.0]
root         676  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.0.1]
root         677  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.1.1]
root         678  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.2.1]
root         679  0.0  0.0      0     0 ?        S    13:43   0:00 [comp_1.3.1]
root         680  0.0  0.0      0     0 ?        S    13:43   0:00 [sdma0]
root         681  0.0  0.0      0     0 ?        S    13:43   0:00 [vcn_dec]
root         682  0.0  0.0      0     0 ?        S    13:43   0:00 [vcn_enc0]
root         683  0.0  0.0      0     0 ?        S    13:43   0:00 [vcn_enc1]
root         684  0.0  0.0      0     0 ?        S    13:43   0:00 [jpeg_dec]
root         691  0.0  0.1 248640  7848 ?        Ssl  13:43   0:00 /usr/libexec/accounts-daemon
root         692  0.0  0.0   2812  1124 ?        Ss   13:43   0:00 /usr/sbin/acpid
avahi        694  0.0  0.0   7708  3348 ?        Ss   13:43   0:00 avahi-daemon: running [tejeswar-Aspi
root         695  0.0  0.0  10592  5220 ?        Ss   13:43   0:00 /usr/lib/bluetooth/bluetoothd
root         697  0.0  0.0  18148  2912 ?        Ss   13:43   0:00 /usr/sbin/cron -f -P
message+     698  0.0  0.0  11112  6608 ?        Ss   13:43   0:02 @dbus-daemon --system --address=syst
root         700  0.0  0.2 269728 18780 ?        Ssl  13:43   0:01 /usr/sbin/NetworkManager --no-daemon
root         706  0.0  0.0  82852  3844 ?        Ssl  13:43   0:00 /usr/sbin/irqbalance --foreground
root         709  0.0  0.2  49712 20644 ?        Ss   13:43   0:00 /usr/bin/python3 /usr/bin/networkd-d
root         712  0.0  0.1 251692 10688 ?        Ssl  13:43   0:01 /usr/libexec/polkitd --no-debug
root         715  0.0  0.0 248736  6724 ?        Ssl  13:43   0:00 /usr/libexec/power-profiles-daemon
syslog       717  0.0  0.0 222400  4736 ?        Ssl  13:43   0:00 /usr/sbin/rsyslogd -n -iNONE
root         723  0.1  0.5 2057508 39932 ?       Ssl  13:43   0:12 /usr/lib/snapd/snapd
root         726  0.0  0.0 245128  6604 ?        Ssl  13:43   0:00 /usr/libexec/switcheroo-control
root         728  0.0  0.1  48260  7844 ?        Ss   13:43   0:00 /lib/systemd/systemd-logind
root         730  0.0  0.1 393056 12816 ?        Ssl  13:43   0:00 /usr/libexec/udisks2/udisksd
root         734  0.0  0.1  17816 10116 ?        Ss   13:43   0:00 /sbin/wpa_supplicant -u -s -O /run/w
avahi        745  0.0  0.0   7440   336 ?        S    13:43   0:00 avahi-daemon: chroot helper
root         760  0.0  0.1 317012 11792 ?        Ssl  13:43   0:00 /usr/sbin/ModemManager
root         815  0.0  0.0      0     0 ?        S    13:43   0:00 [UVM global queu]
root         816  0.0  0.0      0     0 ?        S    13:43   0:00 [UVM deferred re]
root         817  0.0  0.0      0     0 ?        S    13:43   0:00 [UVM Tools Event]
nvidia-+     821  0.0  0.0   5292  1852 ?        Ss   13:43   0:00 /usr/bin/nvidia-persistenced --user 
root         844  0.0  0.1  81652 11884 ?        Ss   13:43   0:00 /usr/sbin/cupsd -l
root         883  0.0  0.2 126796 21608 ?        Ssl  13:43   0:00 /usr/bin/python3 /usr/share/unattend
root         886  0.0  0.1 249872  8820 ?        Ssl  13:43   0:00 /usr/sbin/gdm3
root         891  0.0  0.0      0     0 ?        I<   13:43   0:00 [dm_vblank_contr]
root         911  0.0  0.0      0     0 ?        S    13:43   0:00 [card1-crtc0]
root         912  0.0  0.0      0     0 ?        S    13:43   0:00 [card1-crtc1]
root         913  0.0  0.0      0     0 ?        S    13:43   0:00 [card1-crtc2]
root         914  0.0  0.0      0     0 ?        S    13:43   0:00 [card1-crtc3]
rtkit       1048  0.0  0.0 154000  1580 ?        SNsl 13:43   0:00 /usr/libexec/rtkit-daemon
root        1144  0.0  0.1 251132  8508 ?        Ssl  13:43   0:00 /usr/libexec/upowerd
geoclue     1358  0.0  0.3 722800 24632 ?        Ssl  13:44   0:01 /usr/libexec/geoclue
root        1359  0.1  0.7 381568 52592 ?        Ssl  13:44   0:10 /usr/libexec/packagekitd
colord      1490  0.0  0.1 253932 12176 ?        Ssl  13:44   0:00 /usr/libexec/colord
root        1575  0.0  0.1 179368 10652 ?        Sl   13:44   0:00 gdm-session-worker [pam/gdm-password
root        1582  0.0  0.1 172612 11716 ?        Ssl  13:44   0:00 /usr/sbin/cups-browsed
kernoops    1585  0.0  0.0  13080  2364 ?        Ss   13:44   0:00 /usr/sbin/kerneloops --test
kernoops    1590  0.0  0.0  13080   452 ?        Ss   13:44   0:00 /usr/sbin/kerneloops
tejeswar    1594  0.0  0.1  18004 10804 ?        Ss   13:44   0:00 /lib/systemd/systemd --user
tejeswar    1595  0.0  0.0 170120  4212 ?        S    13:44   0:00 (sd-pam)
tejeswar    1601  0.0  0.0  48504  4676 ?        S<sl 13:44   0:00 /usr/bin/pipewire
tejeswar    1602  0.0  0.0  32256  4252 ?        Ssl  13:44   0:00 /usr/bin/pipewire-media-session
tejeswar    1603  2.3  0.3 3865636 25304 ?       S<sl 13:44   2:38 /usr/bin/pulseaudio --daemonize=no -
tejeswar    1613  0.0  0.0 249536  6828 ?        Sl   13:44   0:00 /usr/bin/gnome-keyring-daemon --daem
tejeswar    1617  0.0  0.0   9864  5560 ?        Ss   13:44   0:00 /usr/bin/dbus-daemon --session --add
tejeswar    1624  0.0  0.1 249288  7792 ?        Ssl  13:44   0:00 /usr/libexec/gvfsd
tejeswar    1636  0.0  0.0 380884  6024 ?        Sl   13:44   0:00 /usr/libexec/gvfsd-fuse /run/user/10
tejeswar    1650  0.0  0.0 545556  6952 ?        Ssl  13:44   0:00 /usr/libexec/xdg-document-portal
tejeswar    1654  0.0  0.0 244796  5276 ?        Ssl  13:44   0:00 /usr/libexec/xdg-permission-store
root        1660  0.0  0.0   2792  1024 ?        Ss   13:44   0:00 fusermount3 -o rw,nosuid,nodev,fsnam
tejeswar    1675  0.0  0.2 642060 22408 ?        SNsl 13:44   0:00 /usr/libexec/tracker-miner-fs-3
root        1687  0.0  0.0      0     0 ?        S<   13:44   0:00 [krfcommd]
tejeswar    1688  0.0  0.1 324576  8748 ?        Ssl  13:44   0:00 /usr/libexec/gvfs-udisks2-volume-mon
tejeswar    1693  0.0  0.0 246192  6488 ?        Ssl  13:44   0:00 /usr/libexec/gvfs-gphoto2-volume-mon
tejeswar    1697  0.0  0.1 323848  7896 ?        Ssl  13:44   0:00 /usr/libexec/gvfs-afc-volume-monitor
tejeswar    1702  0.0  0.0 245104  6212 ?        Ssl  13:44   0:00 /usr/libexec/gvfs-mtp-volume-monitor
tejeswar    1706  0.0  0.0 245280  6264 ?        Ssl  13:44   0:00 /usr/libexec/gvfs-goa-volume-monitor
tejeswar    1710  0.0  0.4 569888 34976 ?        Sl   13:44   0:00 /usr/libexec/goa-daemon
tejeswar    1717  0.0  0.1 347044 14300 ?        Sl   13:44   0:00 /usr/libexec/goa-identity-service
tejeswar    1719  0.0  0.0 171076  6060 tty2     Ssl+ 13:44   0:00 /usr/libexec/gdm-x-session --run-scr
tejeswar    1722  3.8  2.3 26869296 175260 tty2  Sl+  13:44   4:17 /usr/lib/xorg/Xorg vt2 -displayfd 3 
tejeswar    1837  0.0  0.1 231680 14096 tty2     Sl+  13:44   0:00 /usr/libexec/gnome-session-binary --
tejeswar    1926  0.0  0.1 309736  7912 ?        Ssl  13:44   0:00 /usr/libexec/at-spi-bus-launcher
tejeswar    1932  0.0  0.0   8560  4216 ?        S    13:44   0:00 /usr/bin/dbus-daemon --config-file=/
tejeswar    1955  0.0  0.0 100556  5012 ?        Ssl  13:44   0:00 /usr/libexec/gnome-session-ctl --mon
tejeswar    1967  0.0  0.2 527832 15896 ?        Ssl  13:44   0:00 /usr/libexec/gnome-session-binary --
tejeswar    1990  4.6  4.0 6857996 300140 ?      Ssl  13:44   5:14 /usr/bin/gnome-shell
tejeswar    2037  0.0  0.2 582752 15560 ?        Sl   13:44   0:00 /usr/libexec/gnome-shell-calendar-se
tejeswar    2043  0.0  0.3 1080776 22924 ?       Ssl  13:44   0:00 /usr/libexec/evolution-source-regist
tejeswar    2053  0.0  0.3 1127628 26808 ?       Ssl  13:44   0:00 /usr/libexec/evolution-calendar-fact
tejeswar    2056  0.0  0.0 156944  5908 ?        Ssl  13:44   0:00 /usr/libexec/dconf-service
tejeswar    2065  0.0  0.3 754596 22884 ?        Ssl  13:44   0:00 /usr/libexec/evolution-addressbook-f
tejeswar    2081  0.0  0.1 323508  7736 ?        Sl   13:44   0:00 /usr/libexec/gvfsd-trash --spawner :
tejeswar    2092  0.0  0.3 2939904 25904 ?       Sl   13:44   0:00 /usr/bin/gjs /usr/share/gnome-shell/
tejeswar    2094  0.0  0.1 162744  8044 ?        Sl   13:44   0:01 /usr/libexec/at-spi2-registryd --use
tejeswar    2109  0.0  0.0   2888  1000 ?        Ss   13:44   0:00 sh -c /usr/bin/ibus-daemon --panel d
tejeswar    2110  0.0  0.0 319068  6628 ?        Ssl  13:44   0:00 /usr/libexec/gsd-a11y-settings
tejeswar    2112  0.0  0.1 323472 11452 ?        Sl   13:44   0:00 /usr/bin/ibus-daemon --panel disable
tejeswar    2114  0.0  0.3 619648 26776 ?        Ssl  13:44   0:00 /usr/libexec/gsd-color
tejeswar    2116  0.0  0.1 384104 14552 ?        Ssl  13:44   0:00 /usr/libexec/gsd-datetime
tejeswar    2118  0.0  0.0 320572  7388 ?        Ssl  13:44   0:00 /usr/libexec/gsd-housekeeping
tejeswar    2121  0.0  0.3 350072 24272 ?        Ssl  13:44   0:00 /usr/libexec/gsd-keyboard
tejeswar    2123  0.0  0.3 873768 27020 ?        Ssl  13:44   0:00 /usr/libexec/gsd-media-keys
tejeswar    2125  0.0  0.3 725088 27788 ?        Ssl  13:44   0:00 /usr/libexec/gsd-power
tejeswar    2129  0.0  0.1 258500 11532 ?        Ssl  13:44   0:00 /usr/libexec/gsd-print-notifications
tejeswar    2131  0.0  0.0 466500  6316 ?        Ssl  13:44   0:00 /usr/libexec/gsd-rfkill
tejeswar    2132  0.0  0.0 244932  6108 ?        Ssl  13:44   0:00 /usr/libexec/gsd-screensaver-proxy
tejeswar    2134  0.0  0.1 474588  8788 ?        Ssl  13:44   0:00 /usr/libexec/gsd-sharing
tejeswar    2135  0.0  0.1 320948  7636 ?        Ssl  13:44   0:00 /usr/libexec/gsd-smartcard
tejeswar    2137  0.0  0.1 327984  8952 ?        Ssl  13:44   0:00 /usr/libexec/gsd-sound
tejeswar    2138  0.0  0.3 350424 24676 ?        Ssl  13:44   0:00 /usr/libexec/gsd-wacom
tejeswar    2140  0.0  0.3 351988 26448 ?        Ssl  13:44   0:00 /usr/libexec/gsd-xsettings
tejeswar    2144  0.0  0.1  76120 11984 ?        Ss   13:44   0:00 /snap/snapd-desktop-integration/49/u
tejeswar    2160  0.0  0.0 232260  6496 ?        Sl   13:44   0:00 /usr/libexec/gsd-disk-utility-notify
tejeswar    2163  0.0  0.1 484968 10568 ?        Ssl  13:44   0:00 /usr/libexec/xdg-desktop-portal
tejeswar    2165  0.0  0.7 735260 58932 ?        Sl   13:44   0:00 /usr/libexec/evolution-data-server/e
tejeswar    2190  0.0  0.0 172128  7172 ?        Sl   13:44   0:00 /usr/libexec/ibus-memconf
tejeswar    2205  0.0  0.3 281204 29004 ?        Sl   13:44   0:01 /usr/libexec/ibus-extension-gtk3
tejeswar    2215  0.0  0.3 202832 24848 ?        Sl   13:44   0:00 /usr/libexec/ibus-x11 --kill-daemon
tejeswar    2221  0.0  0.0 245908  6704 ?        Sl   13:44   0:00 /usr/libexec/ibus-portal
tejeswar    2230  0.0  1.0 2031632 79768 ?       Ssl  13:44   0:00 /usr/libexec/xdg-desktop-portal-gnom
tejeswar    2262  0.0  0.1 351000 13908 ?        Sl   13:44   0:00 /usr/libexec/gsd-printer
tejeswar    2360  0.0  0.3 311936 23940 ?        Sl   13:44   0:00 /snap/snapd-desktop-integration/49/u
tejeswar    2406  0.0  0.3 2939896 23820 ?       Sl   13:44   0:00 /usr/bin/gjs /usr/share/gnome-shell/
tejeswar    2418  0.0  0.0 172128  7228 ?        Sl   13:44   0:00 /usr/libexec/ibus-engine-simple
tejeswar    2438  0.0  0.3 350724 24716 ?        Ssl  13:44   0:00 /usr/libexec/xdg-desktop-portal-gtk
tejeswar    2470  0.0  0.7 3129952 56012 ?       Sl   13:44   0:03 gjs /usr/share/gnome-shell/extension
tejeswar    2493  0.0  0.0 171664  6528 ?        Ssl  13:44   0:00 /usr/libexec/gvfsd-metadata
tejeswar    2598  0.0  0.4 500288 30028 ?        Sl   13:45   0:00 update-notifier
tejeswar    2825  3.8  6.1 4633780 457636 ?      Sl   13:53   3:55 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    3098  0.0  0.4 223012 37196 ?        Sl   13:53   0:00 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    3124  0.1  1.5 2481548 116244 ?      Sl   13:53   0:09 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    3161  0.0  0.3 645068 25724 ?        Sl   13:53   0:00 /usr/bin/snap userd
tejeswar    3323  0.0  1.2 2457060 90748 ?       Sl   13:53   0:00 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    3823  0.0  0.6 379936 49084 ?        Sl   13:54   0:01 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    3825  0.0  0.4 227392 34956 ?        Sl   13:54   0:00 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    4795  0.0  0.7 882256 55332 ?        Sl   13:56   0:00 /usr/bin/gnome-calendar --gapplicati
tejeswar    4933  0.0  0.0   7820  1276 ?        S    13:56   0:00 /usr/bin/zoom
tejeswar    4936 22.9  8.4 8036028 631720 ?      SLl  13:56  22:51 /opt/zoom/zoom 
tejeswar    4946  0.0  1.9 554216 146584 ?       S    13:56   0:00 /opt/zoom/zoom --type=zygote --no-zy
tejeswar    4947  0.0  1.9 554204 146776 ?       S    13:56   0:00 /opt/zoom/zoom --type=zygote --disab
tejeswar    4963  0.0  0.5 554232 39640 ?        S    13:56   0:00 /opt/zoom/zoom --type=zygote --disab
tejeswar    4979  0.0  2.6 3413472 199352 ?      Sl   13:56   0:01 /opt/zoom/zoom --type=gpu-process --
tejeswar    5051  0.0  2.2 1120684 169116 ?      Sl   13:56   0:00 /opt/zoom/zoom --type=utility --util
tejeswar    5159  0.0  0.7 849528 57500 ?        Sl   13:56   0:00 /opt/zoom/zoom --type=utility --util
tejeswar    5166  0.0  1.1 44022768 82840 ?      Sl   13:56   0:00 /opt/zoom/zoom --type=renderer --loc
tejeswar    5167  0.0  2.1 44528084 159620 ?     Sl   13:56   0:01 /opt/zoom/zoom --type=renderer --loc
tejeswar    5390  0.6  2.0 38327412 155388 ?     SLl  13:57   0:38 /usr/share/discord/Discord
tejeswar    5394  0.0  0.6 222096 46548 ?        S    13:57   0:00 /usr/share/discord/Discord --type=zy
tejeswar    5395  0.0  0.6 222096 47128 ?        S    13:57   0:00 /usr/share/discord/Discord --type=zy
tejeswar    5397  0.0  0.1 222096 10240 ?        S    13:57   0:00 /usr/share/discord/Discord --type=zy
tejeswar    5424  2.5  2.3 693724 176080 ?       Sl   13:57   2:34 /usr/share/discord/Discord --type=gp
tejeswar    5432  0.0  0.9 301376 69132 ?        Sl   13:57   0:01 /usr/share/discord/Discord --type=ut
tejeswar    5490  6.0  5.0 114326588 380868 ?    Sl   13:57   5:58 /usr/share/discord/Discord --type=re
tejeswar    5527  0.0  0.6 537604 50776 ?        Sl   13:57   0:00 /usr/share/discord/Discord --type=ut
root        5780  0.0  0.0      0     0 ?        I    13:59   0:00 [kworker/8:0-events]
root        5890  0.0  0.0      0     0 ?        I    14:02   0:00 [kworker/10:2-events]
root        7029  0.2  0.0      0     0 ?        I    14:22   0:09 [kworker/u32:1-events_unbound]
root        7067  0.0  0.0      0     0 ?        I    14:24   0:00 [kworker/0:0-events]
root        7794  0.0  0.0      0     0 ?        I<   14:44   0:00 [kworker/u33:1-rb_allocator]
root        7832  0.0  0.0      0     0 ?        I    14:44   0:00 [kworker/7:0-events]
root        7945  0.0  0.0      0     0 ?        I    14:47   0:00 [kworker/5:2-mm_percpu_wq]
root        8025  0.1  0.0      0     0 ?        R    14:49   0:02 [kworker/u32:2+events_unbound]
tejeswar    8115  0.0  0.8 1210604 65216 ?       Sl   14:51   0:02 /usr/bin/nautilus --gapplication-ser
root        8324  0.0  0.0      0     0 ?        I    14:52   0:00 [kworker/6:1-events_freezable]
tejeswar    8360  0.5  2.1 2536136 157304 ?      Sl   14:53   0:15 /snap/firefox/2277/usr/lib/firefox/f
tejeswar    8684  0.6  0.7 904660 52652 ?        Rsl  14:57   0:15 /usr/libexec/gnome-terminal-server
tejeswar    8704  0.0  0.0  20056  5144 pts/0    Ss   14:57   0:00 bash
root        8811  0.0  0.0      0     0 ?        I    15:00   0:00 [kworker/11:0-events]
tejeswar    8896  1.6  3.6 1439524 274688 ?      Sl   15:02   0:34 /snap/snap-store/638/usr/bin/snap-st
root        9003  0.0  0.9 449088 72632 ?        Ssl  15:02   0:01 /usr/libexec/fwupd/fwupd
tejeswar   10423  1.7  3.3 34120616 249752 ?     SLl  15:03   0:35 /opt/google/chrome/chrome
tejeswar   10428  0.0  0.0  17164  1076 ?        S    15:03   0:00 cat
tejeswar   10429  0.0  0.0  17164  1020 ?        S    15:03   0:00 cat
tejeswar   10431  0.0  0.0 33575928 3224 ?       Sl   15:03   0:00 /opt/google/chrome/chrome_crashpad_h
tejeswar   10433  0.0  0.0 33567716 1576 ?       Sl   15:03   0:00 /opt/google/chrome/chrome_crashpad_h
tejeswar   10439  0.0  0.8 33863432 60360 ?      S    15:03   0:00 /opt/google/chrome/chrome --type=zyg
tejeswar   10440  0.0  0.8 33863424 60424 ?      S    15:03   0:00 /opt/google/chrome/chrome --type=zyg
tejeswar   10441  0.0  0.0 33568336 4944 ?       S    15:03   0:00 /opt/google/chrome/nacl_helper
tejeswar   10444  0.0  0.2 33863448 16440 ?      S    15:03   0:00 /opt/google/chrome/chrome --type=zyg
tejeswar   10468  1.9  3.1 34317856 238876 ?     Sl   15:03   0:37 /opt/google/chrome/chrome --type=gpu
tejeswar   10469  0.3  1.4 33914120 106400 ?     Sl   15:03   0:06 /opt/google/chrome/chrome --type=uti
tejeswar   10480  0.0  0.6 33904828 46624 ?      Sl   15:03   0:00 /opt/google/chrome/chrome --type=uti
tejeswar   11600  0.0  0.9 34154620 71324 ?      Sl   15:04   0:00 /opt/google/chrome/chrome --type=uti
tejeswar   11753  0.3  2.1 1184815912 160404 ?   Sl   15:04   0:06 /opt/google/chrome/chrome --type=ren
tejeswar   12131  0.0  0.1 290208 10536 ?        Sl   15:06   0:00 /usr/libexec/gvfsd-http --spawner :1
tejeswar   12431  1.5  2.4 1184849572 179832 ?   Sl   15:06   0:27 /opt/google/chrome/chrome --type=ren
tejeswar   12479  0.0  0.8 2419400 64032 ?       Sl   15:07   0:00 /snap/firefox/2277/usr/lib/firefox/f
root       12597  0.0  0.0      0     0 ?        I    15:10   0:00 [kworker/6:0-mm_percpu_wq]
tejeswar   12644  0.0  0.8 2419408 63772 ?       Sl   15:11   0:00 /snap/firefox/2277/usr/lib/firefox/f
root       12669  0.0  0.0      0     0 ?        I    15:11   0:00 [kworker/10:0-events]
root       12738  0.0  0.0      0     0 ?        I    15:12   0:00 [kworker/8:1-events]
tejeswar   12891  0.3  1.6 1184816332 125280 ?   Sl   15:14   0:05 /opt/google/chrome/chrome --type=ren
tejeswar   13155  0.0  0.8 1184739108 63064 ?    Sl   15:18   0:00 /opt/google/chrome/chrome --type=ren
root       13225  0.0  0.0      0     0 ?        I    15:20   0:00 [kworker/9:2-events]
root       13247  0.0  0.0      0     0 ?        I    15:21   0:00 [kworker/4:1-mm_percpu_wq]
tejeswar   13268  0.0  0.0  20056  5492 pts/1    Ss+  15:23   0:00 bash
root       13413  0.0  0.0      0     0 ?        I    15:24   0:00 [kworker/11:1-events]
root       13470  0.0  0.0      0     0 ?        I    15:26   0:00 [kworker/7:2-events]
root       13492  0.0  0.0      0     0 ?        I    15:28   0:00 [kworker/0:2-events]
root       13502  0.0  0.0      0     0 ?        I    15:29   0:00 [kworker/5:1-events]
root       13513  0.0  0.0      0     0 ?        I    15:29   0:00 [kworker/9:1-events]
tejeswar   13515  0.0  0.8 2419952 62044 ?       Sl   15:29   0:00 /snap/firefox/2277/usr/lib/firefox/f
root       13543  0.0  0.0      0     0 ?        I    15:29   0:00 [kworker/3:0-events]
root       13544  0.0  0.0      0     0 ?        I    15:29   0:00 [kworker/u32:0-events_unbound]
root       13555  0.0  0.0      0     0 ?        I    15:29   0:00 [kworker/4:0-events]
root       13557  0.0  0.0      0     0 ?        I    15:29   0:00 [kworker/2:1]
root       13563  0.0  0.0      0     0 ?        I    15:30   0:00 [kworker/1:2-events]
root       13584  0.0  0.0      0     0 ?        I    15:31   0:00 [kworker/7:1-events]
root       13601  0.0  0.0      0     0 ?        I    15:33   0:00 [kworker/8:2]
root       13607  0.0  0.0      0     0 ?        I    15:34   0:00 [kworker/5:0]
root       13623  0.0  0.0      0     0 ?        I    15:35   0:00 [kworker/0:1]
root       13627  0.0  0.0      0     0 ?        I    15:35   0:00 [kworker/4:2-events]
root       13629  0.0  0.0      0     0 ?        I    15:35   0:00 [kworker/1:0-events]
root       13630  0.0  0.0      0     0 ?        I    15:36   0:00 [kworker/7:3]
tejeswar   13633  0.0  0.0  21324  3508 pts/0    R+   15:36   0:00 ps aux
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ ps -eo pid,user,ppid,cmd,%mem,%cpu --sort=-%cpu |grep firefox
   2825 tejeswar    1990 /snap/firefox/2277/usr/lib/  6.1  3.7
   8360 tejeswar    2825 /snap/firefox/2277/usr/lib/  2.1  0.5
   3124 tejeswar    2825 /snap/firefox/2277/usr/lib/  1.5  0.1
   3098 tejeswar    2825 /snap/firefox/2277/usr/lib/  0.4  0.0
   3323 tejeswar    2825 /snap/firefox/2277/usr/lib/  1.2  0.0
   3823 tejeswar    2825 /snap/firefox/2277/usr/lib/  0.6  0.0
   3825 tejeswar    2825 /snap/firefox/2277/usr/lib/  0.4  0.0
  12479 tejeswar    2825 /snap/firefox/2277/usr/lib/  0.8  0.0
  12644 tejeswar    2825 /snap/firefox/2277/usr/lib/  0.8  0.0
  13515 tejeswar    2825 /snap/firefox/2277/usr/lib/  0.8  0.0
  13665 tejeswar    8704 grep --color=auto firefox    0.0  0.0
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ 
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ whoami
tejeswar
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ id tejeswar
uid=1000(tejeswar) gid=1000(tejeswar) groups=1000(tejeswar),4(adm),24(cdrom),27(sudo),30(dip),46(plugdev),122(lpadmin),134(lxd),135(sambashare)
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ uname -a
Linux tejeswar-Aspire-A715-42G 5.15.0-43-generic #46-Ubuntu SMP Tue Jul 12 10:30:17 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ lspci |grep "audio"
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ lspci |grep -i "audio"
05:00.1 Audio device: Advanced Micro Devices, Inc. [AMD/ATI] Renoir Radeon High Definition Audio Controller
05:00.5 Multimedia controller: Advanced Micro Devices, Inc. [AMD] Raven/Raven2/FireFlight/Renoir Audio Processor (rev 01)
05:00.6 Audio device: Advanced Micro Devices, Inc. [AMD] Family 17h (Models 10h-1fh) HD Audio Controller
tejeswar@tejeswar-Aspire-A715-42G:~/d1$ cd 
tejeswar@tejeswar-Aspire-A715-42G:~$ find ./ -iname *java* 
./.config/discord/0.0.24/modules/discord_spellcheck/node_modules/node-addon-api/doc/working_with_javascript_values.md
tejeswar@tejeswar-Aspire-A715-42G:~$ ls -al
total 160
drwxr-x--- 28 tejeswar tejeswar  4096 Jan 31 15:33 .
drwxr-xr-x  5 root     root      4096 Jan 27 07:35 ..
-rw-rw-r--  1 tejeswar tejeswar    26 Jan 31 09:20 aa.txt
-rw-rw-r--  1 tejeswar tejeswar     0 Jan 26 00:37 a.xt
-rw-------  1 tejeswar tejeswar 15903 Jan 31 10:59 .bash_history
-rw-r--r--  1 tejeswar tejeswar   220 Jan 24 18:48 .bash_logout
-rw-r--r--  1 tejeswar tejeswar  3810 Jan 29 17:09 .bashrc
drwx------ 16 tejeswar tejeswar  4096 Jan 31 15:03 .cache
drwx------ 18 tejeswar tejeswar  4096 Jan 31 15:03 .config
drwxrwxr-x  3 tejeswar tejeswar  4096 Jan 31 15:22 d1
drwxrwxrwx  2 tejeswar tejeswar  4096 Jan 30 22:07 d2
drwxrwxr-x  4 tejeswar tejeswar  4096 Jan 31 01:58 d3
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Desktop
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Documents
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 31 14:53 Downloads
-rw-rw-r--  1 tejeswar tejeswar    68 Jan 29 17:02 environment
-rw-rw-r--  1 tejeswar tejeswar    79 Jan 25 22:02 .gitconfig
drwx------  2 tejeswar tejeswar  4096 Jan 31 15:03 .gnupg
drwxrwxrwx  5 tejeswar tejeswar  4096 Jan 30 18:26 hello
drwxrwxr-x  3 tejeswar tejeswar  4096 Jan 29 17:40 hi
-rw-------  1 tejeswar tejeswar    39 Jan 31 15:33 .lesshst
drwx------  3 tejeswar tejeswar  4096 Jan 25 00:29 .local
drwxrwxr-x  8 tejeswar tejeswar  4096 Jan 31 14:58 mountbluetasks
drwx------  3 tejeswar tejeswar  4096 Jan 25 09:20 .mozilla
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Music
drwxrwxr-x  2 tejeswar tejeswar  4096 Jan 25 12:51 mypractice
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Pictures
drwx------  3 tejeswar tejeswar  4096 Jan 25 11:41 .pki
-rw-r--r--  1 tejeswar tejeswar   807 Jan 24 18:48 .profile
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Public
drwxrwxr-x  4 tejeswar tejeswar  4096 Jan 29 17:30 rough
drwx------  5 tejeswar tejeswar  4096 Jan 25 09:26 snap
drwx------  2 tejeswar tejeswar  4096 Jan 29 17:45 .ssh
-rw-r--r--  1 tejeswar tejeswar     0 Jan 25 11:36 .sudo_as_admin_successful
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Templates
drwx------  6 tejeswar tejeswar  4096 Jan 25 09:20 .thunderbird
-rw-rw-r--  1 tejeswar tejeswar   905 Jan 26 18:04 uhi
drwxr-xr-x  2 tejeswar tejeswar  4096 Jan 25 00:29 Videos
drwx------  8 tejeswar tejeswar  4096 Jan 31 14:49 .zoom
tejeswar@tejeswar-Aspire-A715-42G:~$ cd mountbluetasks
tejeswar@tejeswar-Aspire-A715-42G:~/mountbluetasks$ ls
2023-01-31  2023-02-02  drillone.txt  harry.txt  js.txt    pra.txt
2023-02-01  drillone    drilltwo.txt  hello      practice  yu.txt
tejeswar@tejeswar-Aspire-A715-42G:~/mountbluetasks$ grep -io harry.txt|wc -l
^C
tejeswar@tejeswar-Aspire-A715-42G:~/mountbluetasks$ grep -io harry  harry.txt|wc -l
3172
tejeswar@tejeswar-Aspire-A715-42G:~/mountbluetasks$ 
```

